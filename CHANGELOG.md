[0.1.0]
* Initial version for LibreTranslate 1.3.8

[0.2.0]
* Remove proxy auth
* Enable API keys by default

[1.0.0]
* Initial stable release
* Update LibreTranslate to 1.3.9

[1.0.1]
* Update LibreTranslate to 1.3.10
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.3.10)
* Upgrade to Argos Translate 1.8 by @argosopentech in #400
* Fix metrics endpoint in non-gunicorn processes by @pierotofy in #403
* Uninitialized variable error in `check_command` function - Fixed by @FledPaul in #408

[1.0.2]
* Update LibreTranslate to 1.3.11
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.3.11)
* Update README by @Mortein in #415
* Split parameters into logical blocks, add some notes by @Tampa in #420
* Reduce docker image size by @TheTakylo in #425
* Translated using Weblate (Korean) by @chun92 in #426
* Fix docker cuda image build by @TheTakylo in #427
* Fix bad help message in manage.py by @mnzaki in #428
* Reviewed Esperanto translation by @jorgesumle in #431

[1.0.3]
* Update LibreTranslate to 1.3.12
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.3.12)
* Add trans.zillyhuhn.com mirror by @ChillerDragon in #451
* docs: add contributing guide by @SethFalco in #453
* chore: clean up theming by @SethFalco in #454
* fix(css): fix issues visual issues with dark theme by @SethFalco in #457
* fix: request and push notification on file translations by @SethFalco in #460

[1.1.0]
* Update LibreTranslate to 1.3.13
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.3.13)
* Allow json submission to /suggest by @rrgeorge in #504
* Add option to update models rather than reinstall by @rrgeorge in #503
* Update CTranslate2 to v3 by @argosopentech in #506
* Don't require API key for suggestions by @argosopentech in #507
* Add my mirror (libretranslate.eownerdead.dedyn.io) by @eownerdead in #512

[1.2.0]
* Update LibreTranslate to 1.4.0
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.4.0)
* Add Ukrainian translation by @Fqwe1 in #516
* build(docker): update dockerignore by @CandiedCode in #519
* ci(actions): update github actions to the latest versions by @CandiedCode in #520

[1.2.1]
* Update LibreTranslate to 1.4.1
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.4.1)

[1.3.0]
* Update LibreTranslate to 1.5.0
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.5.0)
* Use lingua for language detection by @pierotofy in https://github.com/LibreTranslate/LibreTranslate/pull/526
* Use langdetect by @pierotofy in https://github.com/LibreTranslate/LibreTranslate/pull/528

[1.3.1]
* Update LibreTranslate to 1.5.1
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.5.1)
* Fix batch auto language detection by @pierotofy in #533

[1.3.2]
* Update LibreTranslate to 1.5.2
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.5.2)
* Add hourly req limit option by @pierotofy in #537
* Pin torch to 2.0.1 by @pierotofy in #540

[1.3.3]
* Update LibreTranslate to 1.5.3
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.5.3)
* Dynamic limiter costs by @pierotofy in https://github.com/LibreTranslate/LibreTranslate/pull/541
* Add hourly decay limit by @pierotofy in https://github.com/LibreTranslate/LibreTranslate/pull/542
* Bump actions/setup-python from 4 to 5 by @dependabot in https://github.com/LibreTranslate/LibreTranslate/pull/551
* Add k8s.yaml to kubernetes section of readme.md by @rasos in https://github.com/LibreTranslate/LibreTranslate/pull/553
* Workaround for salad by @pierotofy in https://github.com/LibreTranslate/LibreTranslate/pull/554
* Update README.md with Helm installation instructions by @drivard in https://github.com/LibreTranslate/LibreTranslate/pull/555
* Removed helm chart directory. It is moved to its own repository. Upda… by @drivard in https://github.com/LibreTranslate/LibreTranslate/pull/557
* build(docker): add arm version by @euberdeveloper in https://github.com/LibreTranslate/LibreTranslate/pull/561
* Added Ruby API wrapper by @SebouChu in https://github.com/LibreTranslate/LibreTranslate/pull/563

[1.3.4]
* Update LibreTranslate to 1.5.4
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.5.4)
* build(sca): :lock: update dependencies to address security issues by @CandiedCode in https://github.com/LibreTranslate/LibreTranslate/pull/570
* Mark Czech as reviewed by @nijel in https://github.com/LibreTranslate/LibreTranslate/pull/572

[2.0.0]
* This requires manual interaction after update to make languages available which are not EN or DE
* Follow the steps from the [documentation](https://docs.cloudron.io/apps/libretranslate/#languages) to install required languages.
* Allow to manually specify the language models to be downloaded, this may drastically reduce required disk space.

[2.0.1]
* Update LibreTranslate to 1.5.5
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.5.5)
* Granular API key char limit support

[2.0.2]
* Update LibreTranslate to 1.5.6
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.5.6)
* ltmanage in bin by @jmlord in https://github.com/LibreTranslate/LibreTranslate/pull/580
* Add --req-time-cost by @pierotofy in https://github.com/LibreTranslate/LibreTranslate/pull/584
* Improve model contributing doc by @argosopentech in https://github.com/LibreTranslate/LibreTranslate/pull/585
* Added TypeScript language binding library. by @tderflinger in https://github.com/LibreTranslate/LibreTranslate/pull/590
* Specify the supported color schemes by @Phoenix616 in https://github.com/LibreTranslate/LibreTranslate/pull/593
* add: R binding by @myanesp in https://github.com/LibreTranslate/LibreTranslate/pull/594
* fix: make libretranslate works with ipv6 by @cyrinux in https://github.com/LibreTranslate/LibreTranslate/pull/596

[2.0.3]
* Update LibreTranslate to 1.5.7
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.5.7)
* Documentation: /detect returns an integer 0-100, not a float by @zachdecook in #603
* Mark hungarian a reviewed by @Netesfiu in #606
* update readme.md(add language List) by @steve15963 in #607
* Fix localization on docker/pip by @pierotofy in #608

[2.0.4]
* Use python virtual env

[2.1.0]
* Update LibreTranslate to 1.6.0
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.6.0)
* Update Argos Translate to v1.9.4 by @PJ-Finlay in #615
* Update Dockerfile base image to Python v3.11.9 by @PeterDaveHello in #619
* Upgrade to Argos Translate v1.9.6 by @PJ-Finlay in #618
* Update cuda.Dockerfile to CUDA 12 by @savionlee in #622
* Support for alternative translations by @pierotofy in #630

[2.1.1]
* Update LibreTranslate to 1.6.1
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.6.1)
* Add a customized LibreTranslate instance to the README.md by @lotigara in #634
* Fix typo in README.md by @ajoga in #646
* cuda dockerfile does not have a "venv/" folder by @justinmdriggers in #648
* removed non-functional pip command by @justinmdriggers in #654
* Update locales by @pierotofy in #657
* Update meta.json for ja by @SA99100 in #658
* docs: update README.md by @eltociear in #663
* Update README.md by @BoFFire in #677

[2.1.2]
* Update LibreTranslate to 1.6.2
* [Full changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.6.2)
* chore: update app.js.template by @eltociear in #686
* Update zh meta.json by @CHminggao in #690
* All locales updated (including Basque and Galician) by @urtzai in #697
* add: Basque language added to the README UI language list by @urtzai in #698
* Reviewed Irish/Gaeilge Translation by @aindriu80 in #700
* Add gd locale by @gunchleoc in #702
* Fix English language name for uk locale by @gunchleoc in #701
[2.1.3]
* Update libretranslate to 1.6.3
* [Full Changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.6.3)
* Update README.md by [@&#8203;Koberum](https://github.com/Koberum) in https://github.com/LibreTranslate/LibreTranslate/pull/708
* Take down offline instance trans.zillyhuhn.com by [@&#8203;ChillerDragon](https://github.com/ChillerDragon) in https://github.com/LibreTranslate/LibreTranslate/pull/709
* Updated mirrors by [@&#8203;vdbhb59](https://github.com/vdbhb59) in https://github.com/LibreTranslate/LibreTranslate/pull/720
* Reviewed the Dutch language! by [@&#8203;DatIsVinnie](https://github.com/DatIsVinnie) in https://github.com/LibreTranslate/LibreTranslate/pull/722
* Updates - MR to merge by [@&#8203;vdbhb59](https://github.com/vdbhb59) in https://github.com/LibreTranslate/LibreTranslate/pull/723
* Upgrade Lexilang package to the last version by [@&#8203;urtzai](https://github.com/urtzai) in https://github.com/LibreTranslate/LibreTranslate/pull/725
* Upgrade torch by [@&#8203;pierotofy](https://github.com/pierotofy) in https://github.com/LibreTranslate/LibreTranslate/pull/726
* [@&#8203;Koberum](https://github.com/Koberum) made their first contribution in https://github.com/LibreTranslate/LibreTranslate/pull/708
* [@&#8203;vdbhb59](https://github.com/vdbhb59) made their first contribution in https://github.com/LibreTranslate/LibreTranslate/pull/720
* [@&#8203;DatIsVinnie](https://github.com/DatIsVinnie) made their first contribution in https://github.com/LibreTranslate/LibreTranslate/pull/722

[2.1.4]
* Update libretranslate to 1.6.4
* [Full Changelog](https://github.com/LibreTranslate/LibreTranslate/releases/tag/v1.6.4)
* Update README.md by [@&#8203;Koberum](https://github.com/Koberum) in https://github.com/LibreTranslate/LibreTranslate/pull/708
* Take down offline instance trans.zillyhuhn.com by [@&#8203;ChillerDragon](https://github.com/ChillerDragon) in https://github.com/LibreTranslate/LibreTranslate/pull/709
* Updated mirrors by [@&#8203;vdbhb59](https://github.com/vdbhb59) in https://github.com/LibreTranslate/LibreTranslate/pull/720
* Reviewed the Dutch language! by [@&#8203;DatIsVinnie](https://github.com/DatIsVinnie) in https://github.com/LibreTranslate/LibreTranslate/pull/722
* Updates - MR to merge by [@&#8203;vdbhb59](https://github.com/vdbhb59) in https://github.com/LibreTranslate/LibreTranslate/pull/723
* Upgrade Lexilang package to the last version by [@&#8203;urtzai](https://github.com/urtzai) in https://github.com/LibreTranslate/LibreTranslate/pull/725
* Upgrade torch by [@&#8203;pierotofy](https://github.com/pierotofy) in https://github.com/LibreTranslate/LibreTranslate/pull/726
* Fixes the lexilang package for language detection
* [@&#8203;Koberum](https://github.com/Koberum) made their first contribution in https://github.com/LibreTranslate/LibreTranslate/pull/708
* [@&#8203;vdbhb59](https://github.com/vdbhb59) made their first contribution in https://github.com/LibreTranslate/LibreTranslate/pull/720
* [@&#8203;DatIsVinnie](https://github.com/DatIsVinnie) made their first contribution in https://github.com/LibreTranslate/LibreTranslate/pull/722

