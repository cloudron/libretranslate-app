### Overview

Free and Open Source Machine Translation API, entirely self-hosted.

The app comes preinstalled with those languages: en,ar,az,ca,zh,cs,da,nl,eo,fi,fr,de,el,he,hi,hu,id,ga,it,ja,ko,fa,pl,pt,ru,sk,es,sv,tr,uk

**The app image is around 16Gb large due to the machine learning models**
