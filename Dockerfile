FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/pkg
WORKDIR /app/pkg

ENV VENV_PATH="/app/code/.venv"
RUN virtualenv -p /usr/bin/python3.10 ${VENV_PATH}
ENV PATH=${VENV_PATH}/bin:$PATH

RUN chown -R cloudron:cloudron /app/code

USER cloudron

# https://pypi.org/project/libretranslate/
# renovate: datasource=pypi depName=libretranslate versioning=pep440
ARG LT_VERSION=1.6.4

RUN source ${VENV_PATH}/bin/activate && \
    ${VENV_PATH}/bin/pip install libretranslate==${LT_VERSION}

COPY install_models.py /app/pkg/

# make folders writable (/home/cloudron/.local)
RUN mkdir -p /home/cloudron/.local/share /home/cloudron/.local/cache && \
    ln -sf /app/data/argos-translate /home/cloudron/.local/share/argos-translate && \
    rm -rf /home/cloudron/.local/cache && ln -s /run/cache /home/cloudron/.local/cache

COPY env.sh.template start.sh /app/pkg/

USER root

CMD [ "/app/pkg/start.sh" ]
