By default, `en` and `de` language packs are pre-installed. To add more languages, see the [documentation](https://docs.cloudron.io/apps/libretranslate/#languages).

By default, the API can be used without a key by anyone. [Generate a key](https://docs.cloudron.io/apps/libretranslate/#api) to lock the app.

