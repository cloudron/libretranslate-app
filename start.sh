#!/bin/bash

set -eu

echo "=> Ensuring directories"
mkdir -p /app/data/db /run/cache /app/data/argos-translate

[[ ! -f /app/data/env.sh ]] && cp /app/pkg/env.sh.template /app/data/env.sh
source /app/data/env.sh
if [[ -z "${LT_LANGUAGE_MODELS:-}" ]]; then
    echo "#export LT_LANGUAGE_MODELS=en,de,ar,az,ca,zh,cs,da,nl,eo,fi,fr,el,he,hi,hu,id,ga,it,ja,ko,fa,pl,pt,ru,sk,es,sv,tr,uk" >> /app/data/env.sh
    echo "export LT_LANGUAGE_MODELS=en,de" >> /app/data/env.sh
    source /app/data/env.sh
fi

echo "=> Change ownership"
chown -R cloudron:cloudron /app/data /run/cache

echo "=> Updating language models"
gosu cloudron:cloudron python3 /app/pkg/install_models.py --load_only_lang_codes "${LT_LANGUAGE_MODELS}" --update

# some directories like sessions (db/sessions) is hardcoded in the code. So switch to data directory
echo "=> Starting LibreTranslate"
cd /app/data
source ${VENV_PATH}/bin/activate
exec /usr/local/bin/gosu cloudron:cloudron ${VENV_PATH}/bin/libretranslate --host 0.0.0.0 --port 5000
