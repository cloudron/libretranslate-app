#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app, apiKey;
    const CONFIG_FILE = "env.sh";

    const messageEn = 'Thanks';
    const messageGe = 'Danke';
    const messageFr = 'Je vous remercie'; //'Merci';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//h3[contains(text(), "Translation API")]'));
        await browser.sleep(2000);
    }

    async function addFrSupport() {
        execSync(`cloudron pull --app ${LOCATION} /app/data/${CONFIG_FILE} /tmp/${CONFIG_FILE}`, EXEC_ARGS);
        execSync(`sed -i'' -e 's;LT_LANGUAGE_MODELS=.*;LT_LANGUAGE_MODELS=en,de,fr;' /tmp/${CONFIG_FILE}`, EXEC_ARGS);
        execSync(`cloudron push --app ${LOCATION} /tmp/${CONFIG_FILE} /app/data/${CONFIG_FILE}`, EXEC_ARGS);
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    }

    async function createApiKey() {
        apiKey = execSync(`cloudron exec --app ${app.id} -- su - cloudron -c '[ -d /app/code/.venv ] && source /app/code/.venv/bin/activate; ltmanage keys --api-keys-db-path /app/data/db/api_keys.db add 120'`, { encoding: 'utf8' }).trim();
        console.log('Created api key', apiKey);
        expect(apiKey.length).to.be.ok();
    }

    async function translateApi(source, target, messageToTranslate, messageToCheck) {
        const response = await superagent.post(`https://${app.fqdn}/translate`)
            .send({ q: messageToTranslate, api_key: apiKey, source, target })
            .ok(() => true);
        expect(response.statusCode).to.be(200);
        expect(response.body.translatedText).to.be(messageToCheck);
    }

    async function translate(source, target, messageToTranslate, messageToCheck) {
        await browser.get(`https://${app.fqdn}/?source=${source}&target=${target}&q=`);
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//textarea[@id="textarea1"]')).sendKeys(messageToTranslate);
        await browser.findElement(By.xpath('//span[contains(text(), "Translate Text")]/ancestor::button')).click();
        await browser.sleep(2000);
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "' + messageToCheck + '")]')), TIMEOUT);
        await browser.sleep(5000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can get Main page', getMainPage);
    it('can add Fr support', addFrSupport);

    it('can translate En -> Ge', translate.bind(null, 'en', 'de', messageEn, messageGe));
    it('can translate En -> Fr', translate.bind(null, 'en', 'fr', messageEn, messageFr));
    it('can create api key', createApiKey);
    it('can make api call', translateApi.bind(null, 'en', 'de', messageEn, messageGe));

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can translate En -> Ge', translate.bind(null, 'en', 'de', messageEn, messageGe));
    it('can translate En -> Fr', translate.bind(null, 'en', 'fr', messageEn, messageFr));
    it('can make api call', translateApi.bind(null, 'en', 'de', messageEn, messageGe));

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });
    it('can translate En -> Ge', translate.bind(null, 'en', 'de', messageEn, messageGe));
    it('can translate En -> Fr', translate.bind(null, 'en', 'fr', messageEn, messageFr));
    it('can make api call', translateApi.bind(null, 'en', 'de', messageEn, messageGe));

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(15000);
    });
    it('can get app information', getAppInfo);
    it('can get Main page', getMainPage);
    it('can translate En -> Ge', translate.bind(null, 'en', 'de', messageEn, messageGe));
    it('can translate En -> Fr', translate.bind(null, 'en', 'fr', messageEn, messageFr));
    it('can make api call', translateApi.bind(null, 'en', 'de', messageEn, messageGe));

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', async function () {
        execSync(`cloudron install --appstore-id com.libretranslate.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can get Main page', getMainPage);
    it('can add Fr support', addFrSupport);
    it('can translate En -> Ge', translate.bind(null, 'en', 'de', messageEn, messageGe));
    it('can translate En -> Fr', translate.bind(null, 'en', 'fr', messageEn, messageFr));
    it('can create api key', createApiKey);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can translate En -> Ge', translate.bind(null, 'en', 'de', messageEn, messageGe));
    it('can translate En -> Fr', translate.bind(null, 'en', 'fr', messageEn, messageFr));
    it('can make api call', translateApi.bind(null, 'en', 'de', messageEn, messageGe));

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
